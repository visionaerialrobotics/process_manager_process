# Brief
The process manager facilitates an efficient activation and deactivation of processes as their use is requested by multiple clients. 
The process manager tries to optimize the set of processes that are active. It uses a reference counter associated to each process requested. When there is a process that is no used by any client (counter = 0) the process is stopped.


# Services
- **start_processes** ([aerostack_msgs/RequestProcesses](https://bitbucket.org/visionaerialrobotics/aerostack_msgs/src/master/srv/RequestProcesses.srv))  
Request to start using a set of processes. 

- **stop_processes** ([aerostack_msgs/RequestProcesses](https://bitbucket.org/visionaerialrobotics/aerostack_msgs/src/master/srv/RequestProcesses.srv))  
Request to stop using a set of processes. 

# Publish topics
- **list_of_active_processes** ([aerostack_msgs/ListOfProcesses](https://bitbucket.org/visionaerialrobotics/aerostack_msgs/src/master/msg/ListOfProcesses.msg))  
Publishes a list of active processes (every time a process is activated or deactivated).

# Tests
The following tests are performed:

* **Test 1:** Start the execution of droneTrajectoryController and droneTrajectoryPlanner.
* **Test 2:** Stop the execution of droneTrajectoryController and droneTrajectoryPlanner.
* **Test 3:** Test the inertial stop algorithm.

Before running tests, the following processes need to be launched:

* roscore
* droneTrajectoryController
* droneTrajectoryPlanner

To start the execution of each tests, the following command has to be executed:
```bash
rostest -tr process_manager_process process_manager_process.launch
```

---
# Contributors
**Maintainer:** Alberto Camporredondo (alberto.camporredondo@gmail.com)  
**Author:** Alberto Camporredondo
