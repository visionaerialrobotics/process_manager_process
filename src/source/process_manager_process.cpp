/*!*******************************************************************************************
 *  \file       process_manager_process.cpp
 *  \brief      ProcessManager implementation file.
 *  \details    This file implements the ProcessManager class.
 *  \authors    Alberto Camporredondo.
 *  \copyright  Copyright (c) 2018 Universidad Politecnica de Madrid
 *              All Rights Reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#include "../include/process_manager_process.h"

/*Constructor*/
ProcessManager::ProcessManager()
{
}

/*Destructor*/
ProcessManager::~ProcessManager()
{
}

/*
 -------------------------
 Robot Process
 -------------------------
*/
void ProcessManager::ownSetUp()
{
  ros::NodeHandle private_nh("~");

  private_nh.param<std::string>("drone_id", drone_id, "1");
  private_nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone" + drone_id);
  private_nh.param<std::string>("my_stack_directory", my_stack_directory,
                                "~/workspace/ros/quadrotor_stack_catkin/src/quadrotor_stack");

  private_nh.param<std::string>("start_processes_srv", start_processes_str, "start_processes");
  private_nh.param<std::string>("stop_processes_srv", stop_processes_str, "stop_processes");
  private_nh.param<std::string>("list_of_active_processes_str", list_of_active_processes_str,
                                "list_of_active_processes");
}

void ProcessManager::ownStart()
{
  start_processes_srv =
      node_handle.advertiseService(start_processes_str, &ProcessManager::startProcessesCallback, this);
  stop_processes_srv = node_handle.advertiseService(stop_processes_str, &ProcessManager::stopProcessesCallback, this);
  list_of_active_processes_pub =
      node_handle.advertise<aerostack_msgs::ListOfProcesses>(list_of_active_processes_str, 1);
}

void ProcessManager::ownStop()
{
  start_processes_srv.shutdown();
  stop_processes_srv.shutdown();
  list_of_active_processes_pub.shutdown();
}

void ProcessManager::ownRun()
{
}

/*
---------------------
 Service Callbacks
---------------------
*/
bool ProcessManager::startProcessesCallback(aerostack_msgs::RequestProcesses::Request& request,
                                            aerostack_msgs::RequestProcesses::Response& response)
{
  if (request.processes.size() == 0)
  {
    stopUnusedProcesses();
    response.acknowledge = true;
    response.error_message = "";
    return true;
  }

  std::cout << "Starting processes..." << std::endl;
  /*Local params*/
  bool _starting_error = false;

  /*Verify if a requested process is already activated*/
  std::vector<std::string> _processes_to_be_activated;
  for (std::string process : request.processes)
  {
    if (processIsActive(process))
    {
      std::cout << "\t [OK] " << process << ": process was already activated" << std::endl;
      active_processes[process]++;
    }
    else
    {
      _processes_to_be_activated.push_back(process);
    }
  }

  /*Shutting down processes with counter value 0*/
  stopUnusedProcesses();

  /*Start processes*/
  for (auto process : _processes_to_be_activated)
  {
    /*Activate process*/
    std::string process_service = "/" + drone_id_namespace + "/" + process + "/start";
    ros::ServiceClient start_process_cli = node_handle.serviceClient<std_srvs::Empty>(process_service);
    std_srvs::Empty empty_srv;
    if (!start_process_cli.call(empty_srv))
    {
      /*Process could not be started*/
      std::cout << "\t [ERROR] " << process << ": " << std::endl;
      _starting_error = true;
      response.error_message += "[ERROR] " + process + ": could not be started\n";
      continue;
    }

    /*Process started correctly*/
    active_processes.insert({ process, 1 });
    std::cout << "\t [OK] " << process << ": started correctly" << std::endl;
  }

  /*Publish list of active processes*/
  publishListOfActiveProcesses();

  if (_starting_error)
  {
    response.acknowledge = false;
  }
  else
  {
    response.acknowledge = true;
    response.error_message = "";
  }
  return true;
}

bool ProcessManager::stopProcessesCallback(aerostack_msgs::RequestProcesses::Request& request,
                                           aerostack_msgs::RequestProcesses::Response& response)
{
  std::cout << "Stopping processes..." << std::endl;

  /*Local params*/
  bool _stopping_error = false;

  /*Stop processes*/
  for (auto process : request.processes)
  {
    if (processIsActive(process))
    {
      /*Decrement counter*/
      active_processes[process]--;
      std::cout << "\t [OK] " << process << ": stopped correctly" << std::endl;
    }
    else
    {
      std::cout << "\t [ERROR] " << process << ": is not active" << std::endl;
      _stopping_error = true;
      response.error_message += "[ERROR] " + process + ": is not active\n";
      continue;
    }

    /*IMPORTANT: Processes with counter value as 0 are not stopped here.
                 They conserve some intertia, so they will be stopped within
                 the next set of processes that need to be active*/
  }

  if (_stopping_error)
  {
    response.acknowledge = false;
  }
  else
  {
    response.acknowledge = true;
    response.error_message = "";
  }
  return true;
}

/*
-----------------------
 Private functions
-----------------------
*/
bool ProcessManager::processIsActive(std::string process)
{
  return active_processes.find(process) != active_processes.end();
}

void ProcessManager::publishListOfActiveProcesses()
{
  aerostack_msgs::ListOfProcesses list_of_processes_msg;
  for (auto active_process : active_processes)
    list_of_processes_msg.processes.push_back(active_process.first);

  list_of_active_processes_pub.publish(list_of_processes_msg);
}

void ProcessManager::stopUnusedProcesses()
{
  for (auto process_pair : active_processes)
  {
    if (process_pair.second == 0)
    {
      /*Shutdown process*/
      std::string process_service = process_pair.first + "/stop";
      ros::ServiceClient start_process_cli = node_handle.serviceClient<std_srvs::Empty>(process_service);
      std_srvs::Empty empty_srv;

      start_process_cli.call(empty_srv);

      /*Remove from active_processes list*/
      active_processes.erase(process_pair.first);
    }
  }
}
