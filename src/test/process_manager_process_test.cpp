
/*********************************************************************************
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#include <gtest/gtest.h>
#include "process_manager_process.h"

/*Parameters*/
ros::NodeHandle* nh;

/*---------------------------------------------*/
/*Test 1: Activate processes*/
TEST(StartProcesses, startSimpleProcesses)
{
  /*Create message*/
  aerostack_msgs::RequestProcesses srv;
  srv.request.processes.push_back("droneTrajectoryController");
  srv.request.processes.push_back("droneTrajectoryPlanner");

  /*Calling start_processes service*/
  while (!nh->serviceClient<aerostack_msgs::RequestProcesses>("start_processes").call(srv))
  {
    ros::Duration(1).sleep();
  }

  /*Cheking result*/
  EXPECT_TRUE(srv.response.acknowledge);
}

/*Test 2: Stop processes*/
TEST(StopProcesses, stopSimpleProcesses)
{
  /*Create message*/
  aerostack_msgs::RequestProcesses srv;
  srv.request.processes.push_back("droneTrajectoryController");
  srv.request.processes.push_back("droneTrajectoryPlanner");

  /*Calling start_processes service*/
  while (!nh->serviceClient<aerostack_msgs::RequestProcesses>("stop_processes").call(srv))
  {
    ros::Duration(1).sleep();
  }

  /*Cheking result*/
  EXPECT_TRUE(srv.response.acknowledge);
}

/*Test 3: Stopping with inertia algorithm*/
TEST(Algorithms, stoppingInertiaAlgorithm)
{
  /*Create message*/
  aerostack_msgs::RequestProcesses srv;
  srv.request.processes.push_back("droneTrajectoryPlanner");

  /*Calling start_processes service*/
  while (!nh->serviceClient<aerostack_msgs::RequestProcesses>("start_processes").call(srv))
  {
    ros::Duration(1).sleep();
  }

  /*Calling start_processes service*/
  while (!nh->serviceClient<aerostack_msgs::RequestProcesses>("stop_processes").call(srv))
  {
    ros::Duration(1).sleep();
  }

  /*Create message*/
  aerostack_msgs::RequestProcesses srv2;
  srv2.request.processes.push_back("droneTrajectoryController");

  /*Calling start_processes service*/
  while (!nh->serviceClient<aerostack_msgs::RequestProcesses>("start_processes").call(srv2))
  {
    ros::Duration(1).sleep();
  }

  /*Cheking result*/
  EXPECT_TRUE(srv2.response.acknowledge);
}

/*--------------------------------------------*/
/*  Main  */
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, ros::this_node::getName());
  nh = new ros::NodeHandle;

  return RUN_ALL_TESTS();
}
