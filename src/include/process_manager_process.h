/*!*********************************************************************************
 *  \file       process_manager_process.h
 *  \brief      ProcessManager definition file.
 *  \details    This file contains the ProcessManager declaration.
 *              To obtain more information about it's definition consult
 *              the process_manager_process.cpp file.
 *  \authors    Alberto Camporredondo.
 *  \copyright  Copyright (c) 2018 Universidad Politecnica de Madrid
 *              All Rights Reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#ifndef PROCESS_MANAGER_PROCESS_H
#define PROCESS_MANAGER_PROCESS_H

/*GNU/Linux*/
#include <string>

/*ROS*/
#include <ros/ros.h>

/*Aerostack*/
#include <robot_process.h>

/*Aerostack msgs*/
#include <aerostack_msgs/ListOfProcesses.h>
#include <aerostack_msgs/RequestProcesses.h>

class ProcessManager : public RobotProcess
{
private:                                       /*Process variables*/
  std::map<std::string, int> active_processes; /*Stores every active process*/

private: /*Comunication Variables*/
  /*NodeHandle*/
  ros::NodeHandle node_handle;

  /*Process parameters*/
  std::string drone_id;
  std::string drone_id_namespace;
  std::string my_stack_directory;

  std::string start_processes_str;
  std::string stop_processes_str;
  std::string list_of_active_processes_str;

  /*Process topics*/
  ros::Publisher list_of_active_processes_pub;

  /*Process services*/
  ros::ServiceServer start_processes_srv;
  ros::ServiceServer stop_processes_srv;

public: /*Constructor & Destructor*/
  ProcessManager();
  ~ProcessManager();

private: /*RobotProcess*/
  void ownSetUp();
  void ownStart();
  void ownStop();
  void ownRun();

private: /*Process functions*/
  bool processIsActive(std::string process);
  void publishListOfActiveProcesses();
  void stopUnusedProcesses();

public: /*Service Callbacks*/
  bool startProcessesCallback(aerostack_msgs::RequestProcesses::Request&, aerostack_msgs::RequestProcesses::Response&);
  bool stopProcessesCallback(aerostack_msgs::RequestProcesses::Request&, aerostack_msgs::RequestProcesses::Response&);
};

#endif
